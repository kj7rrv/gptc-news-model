import gptc

amendments = [
    ("1st", "First"),
    ("2nd", "Second"),
    ("3rd", "Third"),
    ("4th", "Fourth"),
    ("5th", "Fifth"),
    ("6th", "Sixth"),
    ("7th", "Seventh"),
    ("8th", "Eighth"),
    ("9th", "Ninth"),
    ("10th", "Tenth"),
    ("11th", "Eleventh"),
    ("12th", "Twelfth"),
    ("13th", "Thirteenth"),
    ("14th", "Fourteenth"),
    ("15th", "Fifteenth"),
    ("16th", "Sixteenth"),
    ("17th", "Seventeenth"),
    ("18th", "Eighteenth"),
    ("19th", "Nineteenth"),
    ("20th", "Twentieth"),
    ("21st", "Twenty-first"),
    ("22nd", "Twenty-second"),
    ("23rd", "Twenty-third"),
    ("24th", "Twenty-fourth"),
    ("25th", "Twenty-fifth"),
    ("26th", "Twenty-sixth"),
    ("27th", "Twenty-seventh"),
]

with open("model.gptc", "rb") as f:
    model = gptc.deserialize(f)

data = {}

for number, name in amendments:
    number_data = model.get(number + " Amendment")
    name_data = model.get(name + " Amendment")

    if number_data and not name_data:
        data[name] = number_data
    elif name_data and not number_data:
        data[name] = name_data
    elif number_data and name_data:
        data[name] = {
            key: (number_data[key] + name_data[key]) / 2
            for key in number_data.keys()
        }

classified_amendments = sorted(data.items(), key=lambda x: x[1]["left"])

print("# Constitutional Amendment Analysis")
print()
print("""This is an analysis of which amendments to the U.S. Constitution are mentioned
more in right- or left-leaning American news sources. Data do not necessarily
correlate with support or opposition for the amendment among right- or
left-leaning Americans.""")
print()
print("| Amendment      | Left  | Right |")
print("+----------------+-------+-------+")
for amendment, data in classified_amendments:
    percent_right = f"{data['right']*100:>4.1f}%"
    percent_left = f"{data['left']*100:>4.1f}%"

    amendment_padding = " "*(14 - len(amendment))
    print(f"| {amendment}{amendment_padding} | {percent_left} | {percent_right} |")
print("+----------------+-------+-------+")
print("| Amendment      | Left  | Right |")
