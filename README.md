# gptc-news-model

A [GPTC](https://git.kj7rrv.com/kj7rrv/gptc) model to classify American news as
right- or left-leaning

## Scripts

No scripts take any arguments.

* `./download.py`: download new articles and add them to the database
* `./compile.py`: compile GPTC model
* `./export.py`: create `build/` directory with files for release
* `./stats.py`: print statistics on article and source counts

## Sources

Inclusion of a site in this model is not an endorsement of the site.

### Left

* ABC News
* The Atlantic
* CBS News
* CNBC
* CNN
* Democracy Now!
* HuffPost (formerly Huffington Post)
* The Intercept
* Los Angeles Times
* PBS NewsHour
* Slate
* The Washington Post

### Right

* The American Conservative
* American Thinker
* Breitbart
* Daily Caller
* Epoch Times
* The Federalist
* Fox News
* LifeSiteNews
* New York Post
* Not the Bee
* One America News Network
* RedState
* Washington Examiner
